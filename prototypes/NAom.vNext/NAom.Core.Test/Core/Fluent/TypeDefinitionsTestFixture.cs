﻿namespace NAom.Core.Fluent
{
  using System.ComponentModel;
  using System.Linq;
  using NUnit.Framework;

  [TestFixture]
  public class TypeDefinitionsTestFixture
  {

    [Test]
    public void FluentTest()
    {

      IAomMetaModel metaModel = AomMetaModel.CreateModel();

      metaModel.CreateEntityType("Test1")
                .WithProperty
                (
                  PropertyTypeBuilders.String("TestString")
                    .WithAttribute(new NameValuePairAttribute("test", "test"))
                    .WithAttribute(new NameValuePairAttribute("test", "test"))
                )
                .WithProperty(PropertyTypeBuilders.Int32("TestInt32"))
                .WithProperty(PropertyTypeBuilders.DateTime("TestDateTime"))
        ;

      metaModel.CreateEntityType("Test2")
                .WithProperty(PropertyTypeBuilders.Boolean("TestBoolean"))
                .WithProperty(PropertyTypeBuilders.UInt32("TestUInt32"))
                .WithProperty(PropertyTypeBuilders.TimeSpan("TestTimeSpan"))
        ;

      metaModel.EntityTypes.Count().ShouldEqual(2);

    }

    [Test]
    public void Example_Using_PropertyType_Attributes()
    {

      IAomMetaModel metaModel = AomMetaModel.CreateModel();

      metaModel.CreateEntityType("Test1")
        .WithStringProperty
        (
          "TestString", 
          new Constrain.Length(50), 
          new Constrain.NotNull(), 
          new Constrain.Regex("[a-zA-Z]*")
         )
        .WithAttribute(new Constrain.Unique("TestString"))
        .WithAttribute(new DisplayNameAttribute("Test 1"))    // system attribute
        ;
      
    }

  }

  public static class Constrain
  {

    public class Length
    {
      public Length(int maxLength)
      {
      }
    }

    public class NotNull
    {
    }

    public class Null
    {
    }

    public class Regex
    {
      public Regex(string expression)
      {
      }
    }

    public class Unique
    {
      public Unique(string propertyType)
      {
      }
    }

  }

}
