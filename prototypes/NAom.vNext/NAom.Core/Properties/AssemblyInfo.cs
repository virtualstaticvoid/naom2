﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("NAom.Core")]
[assembly: AssemblyDescription("")]

[assembly: ComVisible(false)]

// for unit testing
[assembly: InternalsVisibleTo("NAom.Core.Test, PublicKey=00240000048000009400000006020000002400005253413100040000010001006fbbb3c0523373c3ce02d79db314b8706d3051b02ee53e31b143a401e792c8294a22ddf171cf270449d3932cd7fadf275267ddac1c62362d182040ec7f2b70e1c44ebf06a89cf1c7b36a73c07546883a0a57114494b05c08da7a927494a6a287b76fe463960c3e3e1f40503dec42b0bb0183900b6a1fdfcc0ea685d5b4b26dc2", AllInternalsVisible = true)]

