namespace NAom.Core
{
  using System;
  using System.Collections.Generic;

  internal class EntityType : TypeDefinition, IEntityType
  {

    private ICollection<Type> _interfaces;
    private ICollection<IPropertyType> _properties;

    public EntityType(string name, Type baseType)
      : base(name)
    {
      this.BaseType = baseType;
      _interfaces = new List<Type>();
      _properties = new TypeDefinitionCollection<IPropertyType>();
    }

    public Type BaseType { get; private set; }

    public IEnumerable<Type> Interfaces
    {
      get { return _interfaces; }
    }

    public void AddInterface(Type interfaceType)
    {
      _interfaces.Add(interfaceType);
    }

    public void RemoveInterface(Type interfaceType)
    {
      _interfaces.Remove(interfaceType);
    }

    public void ClearInterfaces()
    {
      _interfaces.Clear();
    }

    public IEnumerable<IPropertyType> Properties
    {
      get { return _properties; }
    }

    public void AddProperty(IPropertyType propertyType)
    {
      _properties.Add(propertyType);
    }

    public void RemoveProperty(IPropertyType propertyType)
    {
      _properties.Remove(propertyType);
    }

    public void ClearProperties()
    {
      _properties.Clear();
    }

  }
}