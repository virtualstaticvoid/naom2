namespace NAom.Core
{
  public interface ITypeConverter<TType>
  {
    string ConvertToString(TType instance);
    TType ConvertFromString(string stringToken);
  }
}