namespace NAom.Core
{
  public class NameValuePairAttribute
  {

    public NameValuePairAttribute()
    {
    }

    public NameValuePairAttribute(string name, string value)
    {
      Name = name;
      Value = value;
    }

    public string Name { get; set; }
    public string Value { get; set; }

  }
}