namespace NAom.Core
{
  using System.Collections.Generic;

  public interface ITypeDefinition
  {
    string Name { get; }
    string DisplayName { get; set; }

    IEnumerable<object> Attributes { get; }
    void AddAttribute(object attribute);
    void RemoveAttribute(object attribute);
    void ClearAttributes();
  }
}