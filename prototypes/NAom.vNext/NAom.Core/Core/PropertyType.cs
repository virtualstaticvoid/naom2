namespace NAom.Core
{
  using System;

  internal abstract class PropertyType : TypeDefinition, IPropertyType
  {

    private static readonly Type PropertyTypeGenericType = typeof(PropertyType<>);

    public static IPropertyType CreatePropertyType(string name, Type dataType)
    {
      Type propertyTypeType = PropertyTypeGenericType.MakeGenericType(dataType);
      return (IPropertyType)Activator.CreateInstance(propertyTypeType, name);
    }

    protected PropertyType(string name, Type dataType)
      : base(name)
    {
      DataType = dataType;
    }

    protected PropertyType(string name, Type dataType, bool isReadOnly) :
      this(name, dataType)
    {
      IsReadOnly = isReadOnly;
    }

    public Type DataType { get; private set; }

    public bool IsReadOnly { get; private set; }
    
  }

  internal class PropertyType<TType> : PropertyType
  {

    public PropertyType(string name) : 
      base(name, typeof(TType))
    {
    }

    public PropertyType(string name, bool isReadOnly) :
      base(name, typeof(TType), isReadOnly)
    {
    }

  }

}