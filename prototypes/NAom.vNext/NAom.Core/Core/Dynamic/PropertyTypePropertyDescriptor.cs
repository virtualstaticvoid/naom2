namespace NAom.Core.Dynamic
{
  using System;
  using System.ComponentModel;

  public class PropertyTypePropertyDescriptor : PropertyDescriptor
  {
    
    private readonly IPropertyType _propertyType;

    public PropertyTypePropertyDescriptor(IPropertyType propertyType)
      : base(propertyType.Name, propertyType.GetSystemAttributes())
    {
      _propertyType = propertyType;
    }

    public override bool CanResetValue(object component)
    {
      throw new NotImplementedException();
    }

    public override object GetValue(object component)
    {
      throw new NotImplementedException();
    }

    public override void ResetValue(object component)
    {
      throw new NotImplementedException();
    }

    public override void SetValue(object component, object value)
    {
      throw new NotImplementedException();
    }

    public override bool ShouldSerializeValue(object component)
    {
      throw new NotImplementedException();
    }

    public override Type ComponentType
    {
      get { throw new NotImplementedException(); }
    }

    public override bool IsReadOnly
    {
      get { return _propertyType.IsReadOnly; }
    }

    public override Type PropertyType
    {
      get { return _propertyType.DataType; }
    }

  }
}