namespace NAom.Core
{
  using System;
  using System.Collections.Generic;

  public interface IEntityType : ITypeDefinition
  {
    Type BaseType { get; }

    IEnumerable<Type> Interfaces { get; }
    void AddInterface(Type interfaceType);
    void RemoveInterface(Type interfaceType);
    void ClearInterfaces();
    
    IEnumerable<IPropertyType> Properties { get; }
    void AddProperty(IPropertyType propertyType);
    void RemoveProperty(IPropertyType propertyType);
    void ClearProperties();
  }
}