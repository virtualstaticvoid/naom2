namespace NAom.Core
{
  using System;

  public interface IPropertyType : ITypeDefinition
  {
    Type DataType { get; }
    bool IsReadOnly { get; }
  }
}