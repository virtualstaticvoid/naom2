namespace NAom.Core.Fluent
{
  public interface ITypeDefinitionBuilder<TBuilder> : IHideObjectMembers
    where TBuilder : class, ITypeDefinitionBuilder<TBuilder>
  {
    TBuilder DisplayName(string name);
    TBuilder WithAttribute(object attribute);
    TBuilder WithAttributes(params object[] attributes);
  }
}