namespace NAom.Core.Fluent
{
  using System;

  public static class EntityTypeBuilders
  {

    public static IEntityTypeBuilder CreateEntityType(this IAomMetaModel metaModel, string name)
    {
      if (metaModel == null) throw new ArgumentNullException("metaModel");
      if (String.IsNullOrEmpty(name)) throw new ArgumentNullException("name");

      return metaModel.CreateEntityType(name, typeof(object));
    }

    public static IEntityTypeBuilder CreateEntityType(this IAomMetaModel metaModel, string name, Type baseType)
    {
      if (metaModel == null) throw new ArgumentNullException("metaModel");
      if (String.IsNullOrEmpty(name)) throw new ArgumentNullException("name");
      if (baseType == null) throw new ArgumentNullException("baseType");

      EntityType entityType = new EntityType(name, baseType);
      metaModel.AddEntityType(entityType);
      return new EntityTypeBuilder(entityType);
    }

  }
}