namespace NAom.Core.Fluent
{
  using System;

  internal class EntityTypeBuilder : IEntityTypeBuilder
  {

    public EntityTypeBuilder(IEntityType entityType)
    {
      EntityType = entityType; 
    }

    public IEntityType EntityType { get; private set; }

    public IEntityTypeBuilder DisplayName(string name)
    {
      EntityType.DisplayName = name;
      return this;
    }

    public IEntityTypeBuilder WithInterface<TType>() 
      where TType : class
    {
      return WithInterface(typeof(TType));
    }

    public IEntityTypeBuilder WithInterface(Type interfaceType)
    {
      EntityType.AddInterface(interfaceType);
      return this;
    }

    public IEntityTypeBuilder WithInterfaces(params Type[] interfaceTypes)
    {
      foreach (Type interfaceType in interfaceTypes)
      {
        EntityType.AddInterface(interfaceType);
      }
      return this;
    }

    public IEntityTypeBuilder WithAttribute(object attribute)
    {
      EntityType.AddAttribute(attribute);
      return this;
    }

    public IEntityTypeBuilder WithAttributes(params object[] attributes)
    {
      foreach (object attribute in attributes)
      {
        EntityType.AddAttribute(attribute); 
      }
      return this;
    }

    public IEntityTypeBuilder WithProperty(string name, Type dataType)
    {
      EntityType.AddProperty(PropertyType.CreatePropertyType(name, dataType));
      return this;
    }

    public IEntityTypeBuilder WithProperty(IPropertyTypeBuilder propertyTypeBuilder)
    {
      EntityType.AddProperty(propertyTypeBuilder.PropertyType);
      return this;
    }

    public IEntityTypeBuilder WithProperties(params IPropertyTypeBuilder[] propertyTypeBuilders)
    {
      foreach (IPropertyTypeBuilder propertyTypeBuilder in propertyTypeBuilders)
      {
        EntityType.AddProperty(propertyTypeBuilder.PropertyType);
      }
      return this;
    }

    public IEntityTypeBuilder WithPropertiesOf<TType>() 
      where TType : class
    {
      return WithPropertiesOf(typeof(TType));
    }

    public IEntityTypeBuilder WithPropertiesOf(Type interfaceType)
    {
      foreach (IPropertyType propertyType in InterfaceSupport.DerivePropertyTypes(interfaceType))
      {
        EntityType.AddProperty(propertyType);
      }
      return this;
    }

    public IEntityTypeBuilder WithPropertiesOf(params Type[] interfaceTypes)
    {
      foreach (IPropertyType propertyType in InterfaceSupport.DerivePropertyTypes(interfaceTypes))
      {
        EntityType.AddProperty(propertyType);
      }
      return this;
    }

  }
}