namespace NAom.Core.Fluent
{
  using System;

  public interface IEntityTypeBuilder : ITypeDefinitionBuilder<IEntityTypeBuilder>
  {
    IEntityType EntityType { get; }

    IEntityTypeBuilder WithInterface<TType>()
      where TType : class;
    IEntityTypeBuilder WithInterface(Type interfaceType);
    IEntityTypeBuilder WithInterfaces(params Type[] interfaceTypes);

    IEntityTypeBuilder WithProperty(string name, Type dataType);
    IEntityTypeBuilder WithProperty(IPropertyTypeBuilder propertyTypeBuilder);
    IEntityTypeBuilder WithProperties(params IPropertyTypeBuilder[] propertyTypeBuilders);

    IEntityTypeBuilder WithPropertiesOf<TType>()
      where TType : class;
    IEntityTypeBuilder WithPropertiesOf(Type interfaceType);
    IEntityTypeBuilder WithPropertiesOf(params Type[] interfaceTypes);

  }
}