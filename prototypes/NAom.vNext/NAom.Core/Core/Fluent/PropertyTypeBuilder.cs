namespace NAom.Core.Fluent
{
  internal class PropertyTypeBuilder : IPropertyTypeBuilder
  {

    public PropertyTypeBuilder(IPropertyType propertyType)
    {
      PropertyType = propertyType;
    }

    public IPropertyType PropertyType { get; set; }

    public IPropertyTypeBuilder DisplayName(string name)
    {
      PropertyType.DisplayName = name;
      return this;
    }

    public IPropertyTypeBuilder WithAttribute(object attribute)
    {
      PropertyType.AddAttribute(attribute);
      return this;
    }

    public IPropertyTypeBuilder WithAttributes(params object[] attributes)
    {
      foreach (object attribute in attributes)
      {
        PropertyType.AddAttribute(attribute);
      }
      return this;
    }

  }
}