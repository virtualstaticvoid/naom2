namespace NAom.Core.Fluent
{
  public interface IPropertyTypeBuilder : ITypeDefinitionBuilder<IPropertyTypeBuilder>
  {
    IPropertyType PropertyType { get; }
  }
}