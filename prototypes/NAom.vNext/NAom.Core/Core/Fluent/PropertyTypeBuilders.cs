namespace NAom.Core.Fluent
{
  using System;

  public static class PropertyTypeBuilders
  {

    #region value types

    public static IPropertyTypeBuilder Boolean(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Boolean>(name));
    }
		
    public static IEntityTypeBuilder WithBooleanProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.Boolean(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder Int16(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Int16>(name));
    }

    public static IEntityTypeBuilder WithInt16Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.Int16(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder Int32(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Int32>(name));
    }

    public static IEntityTypeBuilder WithInt32Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.Int32(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder Int64(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Int64>(name));
    }

    public static IEntityTypeBuilder WithInt64Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.Int64(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder UInt16(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<UInt16>(name));
    }

    public static IEntityTypeBuilder WithUInt16Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.UInt16(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder UInt32(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<UInt32>(name));
    }

    public static IEntityTypeBuilder WithUInt32Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.UInt32(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder UInt64(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<UInt64>(name));
    }

    public static IEntityTypeBuilder WithUInt64Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.UInt64(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder Single(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Single>(name));
    }

    public static IEntityTypeBuilder WithSingleProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.Single(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder Double(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Double>(name));
    }

    public static IEntityTypeBuilder WithDoubleProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.Double(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder Decimal(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Decimal>(name));
    }

    public static IEntityTypeBuilder WithDecimalProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.Decimal(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder DateTime(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<DateTime>(name));
    }

    public static IEntityTypeBuilder WithDateTimeProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.DateTime(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder TimeSpan(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<TimeSpan>(name));
    }

    public static IEntityTypeBuilder WithTimeSpanProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.TimeSpan(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder Enum<TEnum>(string name)
      where TEnum : struct // can't constrain TEnum to an enumerated type
    {
      // TODO
      return new PropertyTypeBuilder(new PropertyType<TEnum>(name));
    }

    public static IEntityTypeBuilder WithEnumProperty<TEnum>(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
      where TEnum : struct // can't constrain TEnum to an enumerated type
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.Enum<TEnum>(name).WithAttributes(attributes));
    }

    #endregion

    #region nullable value types

    public static IPropertyTypeBuilder NullableBoolean(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Boolean?>(name));
    }

    public static IEntityTypeBuilder WithNullableBooleanProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableBoolean(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableInt16(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Int16?>(name));
    }

    public static IEntityTypeBuilder WithNullableInt16Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableInt16(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableInt32(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Int32?>(name));
    }

    public static IEntityTypeBuilder WithNullableInt32Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableInt32(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableInt64(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Int64?>(name));
    }

    public static IEntityTypeBuilder WithNullableInt64Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableInt64(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableUInt16(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<UInt16?>(name));
    }

    public static IEntityTypeBuilder WithNullableUInt16Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableUInt16(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableUInt32(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<UInt32?>(name));
    }

    public static IEntityTypeBuilder WithNullableUInt32Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableUInt32(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableUInt64(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<UInt64?>(name));
    }

    public static IEntityTypeBuilder WithNullableUInt64Property(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableUInt64(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableSingle(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Single?>(name));
    }

    public static IEntityTypeBuilder WithNullableSingleProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableSingle(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableDouble(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Double?>(name));
    }

    public static IEntityTypeBuilder WithNullableDoubleProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableDouble(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableDecimal(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<Decimal?>(name));
    }

    public static IEntityTypeBuilder WithNullableDecimalProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableDecimal(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableDateTime(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<DateTime?>(name));
    }

    public static IEntityTypeBuilder WithNullableDateTimeProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableDateTime(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableTimeSpan(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<TimeSpan?>(name));
    }

    public static IEntityTypeBuilder WithNullableTimeSpanProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableTimeSpan(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder NullableEnum<TEnum>(string name)
      where TEnum : struct // can't constrain TEnum to an enumerated type
    {
      // TODO
      return new PropertyTypeBuilder(new PropertyType<TEnum?>(name));
    }

    public static IEntityTypeBuilder WithNullableEnumProperty<TEnum>(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
      where TEnum : struct // can't constrain TEnum to an enumerated type
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.NullableEnum<TEnum>(name).WithAttributes(attributes));
    }

    #endregion

    public static IPropertyTypeBuilder String(string name)
    {
      return new PropertyTypeBuilder(new PropertyType<String>(name));
    }

    public static IEntityTypeBuilder WithStringProperty(this IEntityTypeBuilder entityTypeBuilder, string name, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.String(name).WithAttributes(attributes));
    }

    public static IPropertyTypeBuilder Custom<TCustom>(string name, ITypeConverter<TCustom> converter)
    {
      // TODO
      return new PropertyTypeBuilder(new PropertyType<TCustom>(name));
    }

    public static IEntityTypeBuilder WithCustomProperty<TCustom>(this IEntityTypeBuilder entityTypeBuilder, string name, ITypeConverter<TCustom> converter, params object[] attributes) 
    {
      return entityTypeBuilder.WithProperty(PropertyTypeBuilders.Custom<TCustom>(name, converter).WithAttributes(attributes));
    }

  }
}