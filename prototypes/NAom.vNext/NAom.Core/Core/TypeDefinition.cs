namespace NAom.Core
{
  using System.Collections.Generic;

  internal abstract class TypeDefinition : ITypeDefinition
  {

    private ICollection<object> _attributes;

    protected TypeDefinition(string name)
    {
      Name = name;
      _attributes = new HashSet<object>();
    }

    public string Name { get; private set; }

    public string DisplayName { get; set; }

    public IEnumerable<object> Attributes
    {
      get { return _attributes; }
    }

    public void AddAttribute(object attribute)
    {
      _attributes.Add(attribute);
    }

    public void RemoveAttribute(object attribute)
    {
      _attributes.Remove(attribute);
    }

    public void ClearAttributes()
    {
      _attributes.Clear();
    }

    public override int GetHashCode()
    {
      return Name.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      TypeDefinition that = obj as TypeDefinition;
      return that != null && string.CompareOrdinal(this.Name, that.Name) == 0;
    }

  }
}