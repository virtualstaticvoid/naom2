namespace NAom.Core.Runtime
{
  public interface IDynamicTypeFactoryGenerator
  {
    IDynamicTypeFactory CreateFactory(IEntityType entityType);
  }
}
