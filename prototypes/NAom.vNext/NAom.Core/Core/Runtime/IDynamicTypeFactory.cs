namespace NAom.Core.Runtime
{
  using System;

  public interface IDynamicTypeFactory
  {
    IEntityType EntityType { get; }
    Type GeneratedType { get; }
    object CreateInstance(params object[] args);
  }

  public interface IDynamicTypeFactory<TType> : IDynamicTypeFactory
    where TType : class
  {

    TType CreateInstance();

    TType CreateInstance<TArg1>(TArg1 arg1);

    TType CreateInstance<TArg1, TArg2>(TArg1 arg1, TArg2 arg2);
    
    TType CreateInstance<TArg1, TArg2, TArg3>(TArg1 arg1, TArg2 arg2, TArg3 arg3);
    
    TType CreateInstance<TArg1, TArg2, TArg3, TArg4>(TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4);
    
    TType CreateInstance<TArg1, TArg2, TArg3, TArg4, TArg5>(TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5);

    TType CreateInstance<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>(TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4,
                                                                   TArg5 arg5, TArg6 arg6);

    TType CreateInstance<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7>(TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4,
                                                                          TArg5 arg5, TArg6 arg6, TArg7 arg7);

  }
}
