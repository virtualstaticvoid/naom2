namespace NAom.Core.Runtime
{
  using System;
  using System.Reflection;
  using System.Reflection.Emit;

  internal static class DynamicAssemblyHolder
  {

    public const string BASENAME = "NAom.DynamicAssembly";
    public const string TYPENAME_FORMAT = BASENAME + ".NAom_{0}_{1}_{2}";

    public static AssemblyBuilder AssemblyBuilder { get; private set; }

    public static ModuleBuilder ModuleBuilder { get; private set; }

    static DynamicAssemblyHolder()
    {

      AssemblyName assemblyName = new AssemblyName(BASENAME);
      assemblyName.Version = new Version(1, 0);
      assemblyName.SetPublicKey(typeof(DynamicAssemblyHolder).Assembly.GetName().GetPublicKey());

      AssemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly
        (
          assemblyName,
          AssemblyBuilderAccess.Run
        );

      ModuleBuilder = AssemblyBuilder.DefineDynamicModule(BASENAME);

    }

  }
}
