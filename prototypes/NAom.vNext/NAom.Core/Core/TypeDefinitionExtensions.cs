namespace NAom.Core
{
  using System;
  using System.Linq;

  public static class TypeDefinitionExtensions
  {

    public static Attribute[] GetSystemAttributes(this ITypeDefinition typeDefinition)
    {
      if (typeDefinition == null) throw new ArgumentNullException("typeDefinition");

      return (from attribute in typeDefinition.Attributes
              where typeof(Attribute).IsAssignableFrom(attribute.GetType())
              select attribute).Cast<Attribute>().ToArray();

    }

    public static Attribute[] GetSystemAttributes(this ITypeDefinition typeDefinition, Type attributeTypeFilter)
    {
      if (typeDefinition == null) throw new ArgumentNullException("typeDefinition");
      if (attributeTypeFilter == null) throw new ArgumentNullException("attributeTypeFilter");
      
      if (!typeof(Attribute).IsAssignableFrom(attributeTypeFilter)) 
        throw new ArgumentOutOfRangeException("filterAttributeType", new InvalidCastException());

      return (from attribute in typeDefinition.Attributes
              where attributeTypeFilter.IsAssignableFrom(attribute.GetType())
              select attribute).Cast<Attribute>().ToArray();

    }

  }
}
