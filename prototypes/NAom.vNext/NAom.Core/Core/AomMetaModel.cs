namespace NAom.Core
{
  using System.Collections.Generic;

  public class AomMetaModel : IAomMetaModel
  {

    public static IAomMetaModel CreateModel()
    {
      return new AomMetaModel();
    }

    private readonly TypeDefinitionCollection<IEntityType> _entityTypes;

    private AomMetaModel()
    {
      _entityTypes = new TypeDefinitionCollection<IEntityType>();
    }

    public IEnumerable<IEntityType> EntityTypes
    {
      get { return _entityTypes; }
    }

    public IEntityType this[int index]
    {
      get { return _entityTypes[index]; }
    }

    public IEntityType this[string name]
    {
      get { return _entityTypes[name]; }
    }

    public void AddEntityType(IEntityType entityType)
    {
      _entityTypes.Add(entityType);
    }

    public void RemoveEntityType(IEntityType entityType)
    {
      _entityTypes.Remove(entityType);
    }

    public void ClearEntityTypes()
    {
      _entityTypes.Clear();
    }

  }
}