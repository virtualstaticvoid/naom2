﻿namespace NAom.Core
{
  using System.Collections.ObjectModel;

  internal class TypeDefinitionCollection<TItem> : KeyedCollection<string, TItem>
    where TItem : ITypeDefinition
  {
    
    protected override string GetKeyForItem(TItem item)
    {
      return item.Name;
    }

  }
}
