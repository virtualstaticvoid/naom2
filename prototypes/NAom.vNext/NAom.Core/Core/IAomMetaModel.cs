namespace NAom.Core
{
  using System.Collections.Generic;

  public interface IAomMetaModel
  {

    IEnumerable<IEntityType> EntityTypes { get; }
    IEntityType this[int index] { get; }
    IEntityType this[string name] { get; }
    void AddEntityType(IEntityType entityType);
    void RemoveEntityType(IEntityType entityType);
    void ClearEntityTypes();

  }
}