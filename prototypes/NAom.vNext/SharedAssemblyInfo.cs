using System;
using System.Reflection;
using System.Security;
using System.Resources;

[assembly: AssemblyProduct("NAom")]
[assembly: AssemblyCompany("VirtualStaticVoid")]

[assembly: SecurityTransparent]
[assembly: AllowPartiallyTrustedCallers]

[assembly: AssemblyCopyright("Copyright (C) VirtualStaticVoid 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("0.1.0.*")]
[assembly: AssemblyFileVersion("0.1.0.0")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: CLSCompliant(true)]
[assembly: NeutralResourcesLanguageAttribute("en")]
