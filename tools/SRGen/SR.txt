# This file is used to generate SR.cs and SR.resx files. The copyright notice
# for those files appears here, in this SR.strings file.
#

# Note that if you want to use non-Ascii characters in this file, then you have
# to save this file with a UTF8 signature. This can be achieved by right clicking
# over the file in the solution explorer, select Open With -> Text Editor (with Encoding )
# Select Unicode (UTF-8 with Signature), and save.
#
# Now you will be able to insert and unicode character into the document.
#

# Options are specified as lines starting with "#!"

# Comments are lines starting with ";" or "#"

# To define the SR class public instead of internal (default):
##! accessor_class_accessibility = public

# To specify a static property that will be used to select the appropriate 
# culture for the resources, e.g. Thread.CurrentUICulture, or CultureInfo.Neutral
# to always pick the neutral culture, or a custom class.
##! culture_info = Resources.CultureInfo

# To not generate any class for this .string files, just the resources (e.g. for
# secondary .strings files)
##! generate_class = false

# If this file has a Custom Tool Namespace set, then the tool cannot obtain
# the correct namespace that the resources will be compiled into. In this case
# it is necessary to specify the resrouce namespace. This can be worked out
# by compiling the assembly, then opening in Reflector, and looking at the resources.
#
# For VB.NET the resource namespace will always be the Root Namespace of the project.
#
# For C# the resource namespace will be (For a .strings file nested in two folders):
#   <DefaultNamespace>.<FirstFolder>.<SecondFolder>
# e.g. ProjectDir\Resources\Custom\SR.strings
#    => <DefaultNamespace>.Resources.Custom
#
##! resource_namespace = RootNamespace


#
# To include additional using statements for types referenced in parameters
# supplying the namespace names comma delimited.
#
##! usings=NameSpace1,NameSpace2.NameSpace3

# strings sections have the format
#       [strings{.locale}]
#
# e.g. 
#       [strings]
#       [strings.de]
#       [strings.de-DE]
#       [strings.de-CK]
#
# The first strings section in this file will be used to generate the class,
# so must include all the resource names that will be available.

[strings]
Simple_Message = A simple message
Message_Which_Takes_Parameters(Type type) = A message which has a parameter value of {0}.
